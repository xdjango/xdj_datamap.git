from django.apps import AppConfig


class DataMapConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'xdj_datamap'
    url_prefix = "xdj_datamap",
